#include "Arduino.h"
#include "SevenSegmentTM1637.h"

SevenSegmentTM1637 display(10, 11);

#define morseButton 8
#define yellowLed 7
#define blueLed 6
#define greenLed 5
#define redLed 4
#define buzzer 3

byte ledStrength;
byte buttonState = 0;
long inputLength;
byte amountPressed;

word timeBetweenLetter;
int64_t timeBetweenWord;

String morseCodes;
String convertedMorseCodes;

bool addCharacter;
bool wordFinished = false; //0=niet klaar, 1=klaar

char character;
String prosign;

String morseCode[8];

char characters[] = {
  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
  'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
  'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
  '_'
};

String morseChar[] = {
  ".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..",
  "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-",
  "-.--", "--..", ".----", "..---", "...--", "....-", ".....", "-....", "--...", "---..", "----.", "-----",
  "-..-."
};
  
String prosigns[] = {
    "ATTENTION", "OUT", "VERIFIED", "WAIT", "SAY AGAIN", "CORRECTION"
};

String morseProsigns[] = {
  "-.-.-", ".-.-.", "...-.", ".-...", "..--..", "........"
};

void setup()
{
  Serial.begin(9600);
  pinMode(morseButton, INPUT_PULLUP);
  pinMode(greenLed, OUTPUT);
  pinMode(buzzer, OUTPUT);
  display.begin();
  display.print("   DIT DAH TRANSLATOR   ");
  display.setPrintDelay(500);
  display.clear();
}

String DitOrDah(int length)  //uitleggen wat een routine doet!
{
  String ditOrDah;
  if (length < 750) {
    ditOrDah = ".";
  } else {
    ditOrDah = "-";
  }
  return ditOrDah;
}

void convertMorseCode(String morseString)
{
  byte arrayLength = sizeof(characters) / sizeof(characters[0]);
  for (byte i = 0; i < arrayLength; i++) {
    if (morseString == morseChar[i]) {
      character = characters[i];
    } else if (morseString == morseProsigns[i]) {
      prosign = prosigns[i];
    }
  }
}

void loop()
{
  buttonState = digitalRead(morseButton);
  if (!buttonState) {
    inputLength++;

    tone(buzzer, 800);
    ledStrength = (analogRead(A2) / 4) - 20; //wat doet dit??
    analogWrite(greenLed, ledStrength);

    if (!wordFinished) {
      digitalWrite(blueLed, LOW);
      convertedMorseCodes = "";
      display.clear();
    }
    
    timeBetweenWord = 0;
    timeBetweenLetter = 0;
    addCharacter = true;
    wordFinished = true;
  }
  else {
    digitalWrite(greenLed, LOW);
    noTone(buzzer);
    if (timeBetweenWord++ >= 0 && timeBetweenWord <= 250000) {
      if (timeBetweenLetter++ >= 0 && timeBetweenLetter <= 25000 && amountPressed < 8) {
        if (inputLength > 150) { //waarom 150?
          morseCode[amountPressed] = DitOrDah(inputLength);
          amountPressed++;

          Serial.print("Duration pressed:\t");
          Serial.print(inputLength);
          Serial.print("\t");
          Serial.print("Amount pressed:\t");
          Serial.println(amountPressed);
          Serial.println();
        }
        inputLength = 0;
      
      } else if (addCharacter) {
        for (byte i = 0; i < amountPressed; i++) {
          morseCodes += morseCode[i];
        }
        convertMorseCode(morseCodes);

        Serial.println("Inputted Morse Code:\t" + morseCodes);

        if (character != '\0') {
          convertedMorseCodes += character;
          Serial.print("Translated Morse Code:\t");
          Serial.println(character);
        } else if (prosign != "") {
          convertedMorseCodes += prosign;
          Serial.println("Translated Morse Code:\t" + prosign);
        } else {
          wordFinished = false;
          display.print("NOT EXIST");
        }
        
        Serial.println();
        display.print(convertedMorseCodes);
        
        amountPressed = 0;
        morseCodes = "";
        character = '\0';
        prosign = "";
        addCharacter = false;
      }
    } else if (wordFinished) {
      analogWrite(redLed, 255);
      display.blink();
      digitalWrite(redLed, LOW);
      analogWrite(yellowLed, 255);
      display.print("   " + convertedMorseCodes);
      digitalWrite(yellowLed, LOW);
      analogWrite(blueLed, 255);

      wordFinished = false;
    }
  }
}